import React from "react";

const Input = ({ onChange, onAdd }) => {
  return (
    <div className="add-todo">
      <input
        className="form-control"
        onChange={(e) => onChange(e)}
        name="title"
        id="title"
        type="text"
        placeholder="What To Do?"
      />
      <button className="" onClick={() => onAdd()}>
        Add
      </button>
    </div>
  );
};

export default Input;
