import React from 'react';
import { connect } from "react-redux";

import List from "./List";
import Input from "./Input";

import { addTodo, deleteTodo, updateTodo } from "../actions";

class ToDo extends React.Component {
  onChange = ({ target }) => {
    this.setState({ [target.name]: target.value, "done":"false" });
  };
  onAdd = () => {
    let id = Math.floor(Math.random() * 1000);
    this.props.addTodo({ id, ...this.state });
    /* TODO
      need to clear text
    */
  };
  onDelete = (id) => {
    this.props.deleteTodo(id);
  };
  onChkChange = (id) => {
    this.props.updateTodo(id);
  }

  render() {
    const { todos } = this.props;
    return (
      <div className="">
        <Input
            onChange={(e) => this.onChange(e)}
            onAdd={() => this.onAdd()}
        />
        <List
          todos={todos}
          onDelete={(id) => this.onDelete(id)}
          onChkChange={(id) => this.onChkChange(id)}
        />
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    todos: state.todos,
  };
};

export default connect(mapStateToProps, { addTodo, deleteTodo, updateTodo })(ToDo);