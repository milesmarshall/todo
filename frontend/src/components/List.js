import React from "react";

const List = ({ todos, onDelete, onChkChange }) => {
  return (
    <div className="todo-list">
      {todos && todos.length > 0 ? (
        todos.map((todo) => (
          <div className="todo-item" key={todo.id}>
            <input
              id={`checkbox-${todo.id}`}
              type="checkbox"
              value={todo.done ? false : true}
              onChange={() => onChkChange(todo.id)}
              defaultChecked={todo.done ? false : true}
            />
            <p>{todo.title}</p>
            <button className="" onClick={() => onDelete(todo.id)}>x</button>
            <p>{todo.desc}</p>
          </div>
        ))
      ) : (
        <i>&nbsp;</i>
      )}
    </div>
  );
};

export default List;
