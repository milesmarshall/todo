import React from 'react';
import UserStore from '../../store/index';
import InputField from './InputField';
import SubmitButton from './SubmitButton';

class RegisterForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      email: '',
      buttonDisabled:  false
    }
  }

  setInputValue(property, val) {
    val = val.trim()
    /*if (val.length > 12) {
      return
    }*/
    this.setState({
      [property]: val
    })
  }

  resetForm() {
    this.setState({
      username: '',
      password: '',
      email: '',
      buttonDisabled:  false
    })
  }

  async doRegister() {
    if (!this.state.username) {
      return;
    }
    if (!this.state.password) {
      return;
    }
    if (!this.state.email) {
      return;
    }

    this.setState({
      buttonDisabled: true
    })

    try {
      let res = await fetch('/register', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
          email: this.state.email
        })
      });

      let result = await res.json()
      if (result && result.success) {
        UserStore.isLoggedIn = true
        UserStore.username = result.username
        UserStore.email = result.email
      }
      else if (result && result.success === false) {
        this.resetForm()
        alert(result.msg)
      }
    }

    catch(e) {
      console.log(e)
      this.resetForm()
    }
  }

  render() {
    return (
      <div className="registerForm">
        <InputField
          type='text'
          placeholder='Username'
          value={this.state.username ? this.state.username : ''}
          onChange={ (val) => this.setInputValue('username', val) }
        />
        <InputField
          type='text'
          placeholder='Email'
          value={this.state.email ? this.state.email : ''}
          onChange={ (val) => this.setInputValue('email', val) }
        />
        <InputField
          type='password'
          placeholder='Password'
          value={this.state.password ? this.state.password : ''}
          onChange={ (val) => this.setInputValue('password', val) }
        />
        <SubmitButton
          text='register'
          disabled={this.state.buttonDisabled}
          onClick={ () => this.doRegister() }
        />
      </div>
    );
  }

}

export default RegisterForm;